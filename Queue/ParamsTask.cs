﻿namespace Queue
{
    class ParamsTask
    {
        #region params

        private string countGeneratingOnSide;
        public string CountGeneratingOnSide
        {
            set { countGeneratingOnSide = value; }
            get { return countGeneratingOnSide; }
        }

        private double speedAverage; // Средняя скорость движения автомобиля при старте
        public double SpeedAverage
        {
            get { return speedAverage; }
        }

        private double timeAverage; // временной промежуток между автомобилями
        public double TimeAverage
        {
            get { return timeAverage; }
        }

        private int drivewayCount; //Количество прогонов
        public int DrivewayCount
        {
            get { return drivewayCount; }
        }

        private double lengthSection; //Длина участка дороги, которое требуется проехать автомобилю после выезда из "очереди"
        public double LengthSection
        {
            get { return lengthSection; }
        }

        private int timeRed; //Количество секунд, на котором активен красный свет светофора
        public int TimeRed
        {
            get { return timeRed; }
        }

        #endregion

        #region Constructors

        public ParamsTask()
        {
            timeRed = 55;
            lengthSection = 500;
            drivewayCount = 10;
            timeAverage = 2;
            speedAverage = TranslateKminM(30);
        }

        public ParamsTask(double speedAverage, double lengthSection, int drivewayCount, double timeAverage, int timeRed)
        {
            this.speedAverage = speedAverage;
            this.lengthSection = lengthSection;
            this.timeAverage = timeAverage;
            this.drivewayCount = drivewayCount;
            this.timeRed = timeRed;

            //countSideCars = new List<int>();
        }

        #endregion

        public static double TranslateKminM(double speedofKminHour) => ((speedofKminHour * 1000) / 3600.0);

        public int getTTLs() => (int)(lengthSection / speedAverage);// временной промежуток, требуемый для проезда машиной lengthSection
    }
}
