﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadModelingLab2
{
    class Apply
    {
        private double _timeCreate; //время инициализации 

        private int _indexApp; //номер заявки
        public int IndexApp
        {
            get { return _indexApp; }
            set { _indexApp = value; }
        }

        public Apply(double timeCreate, int indexApp)
        {
            this._timeCreate = timeCreate;
            this._indexApp = indexApp;
        }

        public double timeInQueue(double currTimeSystem) => Math.Abs(currTimeSystem - _timeCreate);
    }
}
